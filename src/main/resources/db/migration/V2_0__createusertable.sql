
CREATE TABLE user(userid INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
category ENUM('Admin', 'User'),
de_register_time DATETIME(6),
email VARCHAR(255) UNIQUE,
first_name VARCHAR(255),
last_name VARCHAR(255),
password VARCHAR(255),
profile TINYBLOB,
user_status TINYINT,
category_name VARCHAR(255));
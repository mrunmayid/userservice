package com.example.userservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class Config {
    public static String passwordEncryptor(String password){
        BCryptPasswordEncoder passwordEncoder= new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    @Bean
    SecurityFilterChain configure(HttpSecurity http) throws Exception{
       http.authorizeHttpRequests(auth->auth.anyRequest().permitAll())
               .csrf(AbstractHttpConfigurer::disable);
       return http.build();
    }
}

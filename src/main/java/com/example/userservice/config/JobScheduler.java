package com.example.userservice.config;


import com.example.userservice.commons.Status;
import com.example.userservice.entity.User;
import com.example.userservice.service.UserService;
import com.example.userservice.utility.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
@Component
public class JobScheduler {

    @Autowired
    UserService userService;

    Logger logger = LoggerFactory.getLogger(JobScheduler.class);


    @Scheduled(cron = "1 * * * * ?")
    public void everyDayTask(){
        logger.info("Scheduler execution Started Now");
        List<User> userList = findTimeDiff(userService.getInactiveUser());
        for (User user:userList){
            user.setUserStatus(Status.ARCHIVE);
            userService.saveUpdatedUser(user);
            logger.info(user.toString());
        }
        logger.info("Deleted All INACTIVE Users.");
    }

    private  List<User> findTimeDiff(List<User> userList) {
        List<User> inactiveUser = new ArrayList<>();
        for (User user: userList){
            LocalDateTime now = Converter.convertToUTC(LocalDateTime.now());
            LocalDateTime tempDateTime = LocalDateTime.from( now );
            long hours = tempDateTime.until( user.getDeRegisterTime(), ChronoUnit.HOURS );
            if((-hours)>=24){
                inactiveUser.add(user);
            }
        }
        return inactiveUser;
    }
}

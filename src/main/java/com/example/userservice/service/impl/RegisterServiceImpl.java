package com.example.userservice.service.impl;

import com.example.userservice.commons.UserDto;
import com.example.userservice.entity.User;
import com.example.userservice.helpers.ResponseHandler;
import com.example.userservice.mapper.MapObject;
import com.example.userservice.repo.UserRepo;
import com.example.userservice.service.RegisterService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Service
public class RegisterServiceImpl implements RegisterService {

    @Autowired
    UserRepo userRepo;
    @Override
    public ResponseEntity<?> saveUser(UserDto userDto){
        try{
            User user = userRepo.save(MapObject.mapUserDtoToUser(userDto));
            return new ResponseEntity<>("User Details::"+user.getEmail(),HttpStatus.CREATED);
        }
        catch (Exception e){
            e.fillInStackTrace();
            return new ResponseEntity<>(new ResponseHandler("Database Error",HttpStatus.INTERNAL_SERVER_ERROR),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @Override
    public boolean hasCsvFormat(MultipartFile multipartFile) {
        String type = "text/csv";
        return type.equals(multipartFile.getContentType());
    }


    @Override
    public boolean processAndSaveData(MultipartFile multipartFile) {
        try {
            List<User> userList = csvToUser(multipartFile.getInputStream());
            if(userList.isEmpty()){
                return false;
            }else{
                userRepo.saveAll(userList);
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private List<User> csvToUser(InputStream inputStream) {
        List<User> userList = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(new InputStreamReader
                (inputStream,"UTF-8"));
            CSVParser csvParser = new CSVParser(reader,
                    CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase()
                            .withTrim());){

            List<CSVRecord> records = csvParser.getRecords();
            for (CSVRecord csvRecord:records) {
                User user = MapObject.mapCSVToUser(csvRecord.get("email")
                        , csvRecord.get("password")
                        , csvRecord.get("first_name")
                        , csvRecord.get(("last_name"))
                        , csvRecord.get("role"));
                userList.add(user);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userList;
    }
}

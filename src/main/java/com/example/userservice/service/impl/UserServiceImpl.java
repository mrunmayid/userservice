package com.example.userservice.service.impl;

import com.example.userservice.commons.Role;
import com.example.userservice.commons.Status;
import com.example.userservice.entity.User;
import com.example.userservice.helpers.ResponseHandler;
import com.example.userservice.mapper.MapObject;
import com.example.userservice.repo.UserRepo;
import com.example.userservice.service.UserService;
import com.example.userservice.utility.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl  implements UserService {

    @Autowired
    UserRepo userRepo;
    @Override
    public ResponseEntity<?> getByUserId(String userId,String admin) {
        if(isAdmin(Integer.parseInt(admin))){
            Optional<User> user = userRepo.findById(Integer.valueOf(userId));
            return isUserPresent(user);
        }else{
            return new ResponseEntity<>(new ResponseHandler("User don't have permission to perform this action",HttpStatus.UNAUTHORIZED),HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public ResponseEntity<?> getByEmail(String email,String admin) {
        if(isAdmin(Integer.parseInt(admin))){
            Optional<User> user = userRepo.findByEmail(email);
            return isUserPresent(user);
        }else {
            return new ResponseEntity<>(new ResponseHandler("User don't have permission to perform this action",HttpStatus.UNAUTHORIZED),HttpStatus.UNAUTHORIZED);
        }

    }

    @Override
    public ResponseEntity<ResponseHandler> blockUser(int userId, int adminId) {
        try {
            if(isAdmin(adminId)){
                Optional<User> user = userRepo.findById(userId);
                if(user.isPresent() && user.get().getUserStatus().equals(Status.ACTIVE ) && user.get().getCategory() == Role.User){
                    User saveUser= user.get();
                    saveUser.setUserStatus(Status.INACTIVE);
                    saveUser.setDeRegisterTime(Converter.convertToUTC(LocalDateTime.now()));
                    userRepo.save(saveUser);
                    return new ResponseEntity<>(new ResponseHandler(" User Blocked Successfully",HttpStatus.OK),HttpStatus.OK);
                }else{
                    return new ResponseEntity<>(new ResponseHandler("User Not Found",HttpStatus.NOT_FOUND),HttpStatus.NOT_FOUND);
                }
            }else {
                return new ResponseEntity<>(new ResponseHandler("User don't have permission to perform this action",HttpStatus.UNAUTHORIZED),HttpStatus.UNAUTHORIZED);
            }
        } catch (Exception e) {
            e.fillInStackTrace();
            return new ResponseEntity<>(new ResponseHandler("Database Error",HttpStatus.INTERNAL_SERVER_ERROR),HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @Override
    public ResponseEntity<?> unBlockUser(int userId, int adminId) {
        try {
            if(isAdmin(adminId)){
                Optional<User> user = userRepo.findById(userId);
                if(user.isPresent() && user.get().getUserStatus().equals(Status.INACTIVE) && user.get().getCategory() == Role.User){
                    User saveUser= user.get();
                    saveUser.setUserStatus(Status.ACTIVE);
                    saveUser.setDeRegisterTime(null);
                    userRepo.save(saveUser);
                    return new ResponseEntity<>(new ResponseHandler(" User Unblocked Successfully",HttpStatus.OK),HttpStatus.OK);
                }else{
                    return new ResponseEntity<>(new ResponseHandler("User Not Found",HttpStatus.NOT_FOUND),HttpStatus.NOT_FOUND);
                }
            }else {
                return new ResponseEntity<>(new ResponseHandler("User don't have permission to perform this action",HttpStatus.UNAUTHORIZED),HttpStatus.UNAUTHORIZED);
            }
        } catch (Exception e) {
            e.fillInStackTrace();
            return new ResponseEntity<>(new ResponseHandler("Database Error",HttpStatus.INTERNAL_SERVER_ERROR),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public List<User> getInactiveUser() {
        return userRepo.findAllInactiveUsers(Status.INACTIVE.ordinal(),Role.User.name());
    }

    @Override
    public void saveUpdatedUser(User user) {
        userRepo.save(user);
    }


    private boolean isAdmin(int adminId) {
        Optional<User> user = userRepo.findById(adminId);
        return user.isPresent() && user.get().getCategory() == Role.Admin;
    }

    private ResponseEntity<?> isUserPresent(Optional<User> user) {
        return user.isPresent() && user.get().getUserStatus()==Status.ACTIVE?
                new ResponseEntity<>(MapObject.mapUserToUserDtoRes(user.get()),HttpStatus.FOUND):
                new ResponseEntity<>(new ResponseHandler("User Not Found",HttpStatus.NOT_FOUND),HttpStatus.NOT_FOUND);
    }
}

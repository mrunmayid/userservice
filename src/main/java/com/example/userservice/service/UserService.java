package com.example.userservice.service;

import com.example.userservice.commons.UserDtoRes;
import com.example.userservice.entity.User;
import com.example.userservice.helpers.ResponseHandler;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {
    ResponseEntity<?> getByUserId(String userId,String adminId);
    ResponseEntity<?> getByEmail(String email, String adminId);
    ResponseEntity<ResponseHandler> blockUser(int userId, int adminId);
    ResponseEntity<?> unBlockUser(int userId, int adminId);

    List<User> getInactiveUser();
    void saveUpdatedUser(User user);
}

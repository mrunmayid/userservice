package com.example.userservice.service;

import com.example.userservice.commons.UserDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface RegisterService {

    public ResponseEntity<?> saveUser(UserDto userDto);

    boolean hasCsvFormat(MultipartFile multipartFile);

    boolean processAndSaveData(MultipartFile multipartFile);
}

package com.example.userservice.commons;

public enum Role {
    Admin,
    User
}
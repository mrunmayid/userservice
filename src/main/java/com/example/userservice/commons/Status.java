package com.example.userservice.commons;

public enum Status {
    INACTIVE,
    ACTIVE,
    ARCHIVE,
}
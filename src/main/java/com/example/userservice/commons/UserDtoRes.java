package com.example.userservice.commons;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDtoRes {
    private int userid;
    private String email;
    private String firstName;
    private String lastName;
    private String profilePicture;
    private String category;
}


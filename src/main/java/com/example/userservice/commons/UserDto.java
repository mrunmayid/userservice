package com.example.userservice.commons;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDto {
    @NotBlank(message = "Invalid email: Empty email")
    @NotNull(message = "Invalid email: Blank email")
    @Email(message = "Invalid email")
    private String email;
    @NotBlank(message = "Invalid password: Empty password")
    @NotNull(message = "Invalid password: Blank password")
    private String password;
    @NotBlank(message = "Invalid Name: Empty name")
    @NotNull(message = "Invalid Name: Blank name")
    private String firstName;
    @NotBlank(message = "Invalid Name: Empty name")
    @NotNull(message = "Invalid Name: Blank name")
    private String lastName;
    private String profilePicture;
    @JsonProperty("isAdmin")
    private boolean isAdmin;
}


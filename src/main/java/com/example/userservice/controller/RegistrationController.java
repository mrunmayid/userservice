package com.example.userservice.controller;

import com.example.userservice.commons.UserDto;
import com.example.userservice.helpers.ResponseHandler;
import com.example.userservice.service.RegisterService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api")
public class RegistrationController {

    @Autowired
    RegisterService registerService;

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody(required = true) UserDto userDto, BindingResult bindingResult) {
        return !bindingResult.hasErrors()? registerService.saveUser(userDto) :
                new ResponseEntity<>(new ResponseHandler("Missing Field",HttpStatus.BAD_REQUEST),HttpStatus.BAD_REQUEST);
    }
    @PostMapping(value = "/registerall", consumes = {"multipart/form-data"})
    public ResponseEntity<?> registerAll(@RequestBody MultipartFile multipartFile){
        if(registerService.hasCsvFormat(multipartFile)){
            return registerService.processAndSaveData(multipartFile)?
                    new ResponseEntity<>(new ResponseHandler("Data Successfully uploaded",HttpStatus.OK),HttpStatus.OK):
                    new ResponseEntity<>(new ResponseHandler("Database Error",HttpStatus.INTERNAL_SERVER_ERROR),HttpStatus.INTERNAL_SERVER_ERROR);
        }
            return new ResponseEntity<>(new ResponseHandler("Try Again..!!!",HttpStatus.EXPECTATION_FAILED),HttpStatus.EXPECTATION_FAILED);

    }
}

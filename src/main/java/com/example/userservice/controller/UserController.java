package com.example.userservice.controller;

import com.example.userservice.commons.UserDtoRes;
import com.example.userservice.helpers.ResponseHandler;
import com.example.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;
    @GetMapping(value = "/get")
    public ResponseEntity<?> getUserByEmailOrUserId(@RequestHeader(required = false, defaultValue = "email") String email,
                                                    @RequestHeader(required = false, defaultValue = "userid") String userId,
                                                    @RequestHeader(required = false, defaultValue = "admin") String adminId) {
        if (!adminId.equals("admin")){
            if (email.equals("email") && userId.equals("userid") ) {
                return new ResponseEntity<>(new ResponseHandler("Empty Fields...!!!", HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
            } else if (email.equals("email")) {
                return userService.getByUserId(userId,adminId);
            } else {
                return userService.getByEmail(email,adminId);
            }
        }
        return new ResponseEntity<>(new ResponseHandler("Admin Fields Empty...!!!", HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);

    }

    @PutMapping("/block")
    public ResponseEntity<?> blockUser(@RequestHeader int userId,@RequestParam int adminId){
        return userService.blockUser(userId,adminId);
    }

    @PutMapping("/unblock")
    public ResponseEntity<?> unblockUser(@RequestHeader int userId,@RequestParam int adminId){
        return userService.unBlockUser(userId,adminId);
    }
}

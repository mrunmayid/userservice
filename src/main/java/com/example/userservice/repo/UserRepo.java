package com.example.userservice.repo;

import com.example.userservice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepo extends JpaRepository<User,Integer> {

    Optional<User> findByEmail(String email);
    @Query(
            value = "SELECT * FROM user u WHERE u.user_status = :status AND u.category = :role",
            nativeQuery = true)
    List<User> findAllInactiveUsers(int status, String role);
}

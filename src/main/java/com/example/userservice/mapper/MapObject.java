package com.example.userservice.mapper;

import com.example.userservice.commons.Role;
import com.example.userservice.commons.Status;
import com.example.userservice.commons.UserDto;
import com.example.userservice.commons.UserDtoRes;
import com.example.userservice.config.Config;
import com.example.userservice.entity.User;
import com.example.userservice.utility.Converter;

import java.time.LocalDateTime;

public class MapObject {

    public static User mapUserDtoToUser(UserDto userDto){
        return User.builder().email(userDto.getEmail())
                .password(Config.passwordEncryptor(userDto.getPassword()))
                 .first_name(userDto.getFirstName())
                .last_name(userDto.getLastName())
                .category(userDto.isAdmin() ? Role.Admin:Role.User )
                .deRegisterTime(Converter.convertToUTC(LocalDateTime.now()))
                .userStatus(Status.ACTIVE)
                .build();
    }


    public static UserDtoRes mapUserToUserDtoRes(User user) {
        return UserDtoRes.builder()
                .userid(user.getUserid())
                .email(user.getEmail())
                .firstName(user.getFirst_name())
                .lastName(user.getLast_name())
                .category(user.getCategory().name())
                .build();
    }


    public static User mapCSVToUser(String email, String password, String firstName, String lastName,
                                    String role) {
        return User.builder()
                .email(email)
                .password(Config.passwordEncryptor(password))
                .first_name(firstName)
                .last_name(lastName)
                .category(role.equals(Role.Admin.name())?Role.Admin:Role.User)
                .userStatus(Status.ACTIVE)
                .build();
    }
}

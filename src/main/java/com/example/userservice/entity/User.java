package com.example.userservice.entity;

import com.example.userservice.commons.Role;
import com.example.userservice.commons.Status;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Table(name = "user")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private int userid;

        @Column(unique = true)
        private String email;

        private String password;

        private String first_name;

        private String last_name;

        @Lob
        private byte[] profile;

        @Enumerated(EnumType.STRING)
        private Role category;

        private Status userStatus;

        private LocalDateTime deRegisterTime;
}


package com.example.userservice.impl;

import com.example.userservice.commons.Role;
import com.example.userservice.commons.Status;
import com.example.userservice.commons.UserDto;
import com.example.userservice.entity.User;
import com.example.userservice.repo.UserRepo;
import com.example.userservice.service.impl.RegisterServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RegisterServiceImplTest {

    @InjectMocks
    RegisterServiceImpl registerService;
    @Mock
    UserRepo userRepo;

    @Test
    void saveUser_Fail() {
        UserDto userDto = createUserDto();
        User user = createUser();
        when(userRepo.save(user)).thenReturn(user);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,registerService.saveUser(userDto).getStatusCode());
    }

    private User createUser() {
        return User.builder()
                .userid(123)
                .first_name("md")
                .last_name("MD")
                .password("password@1234")
                .userStatus(Status.ACTIVE)
                .category(Role.User)
                .profile(null)
                .email("md@bookstore.com")
                .deRegisterTime(null)
                .build();
    }

    private UserDto createUserDto() {
        return UserDto.builder()
                .firstName("md")
                .lastName("MD")
                .password("password@1234")
                .email("md@bookstore.com")
                .isAdmin(true)
                .build();
    }

    @Test
    void hasCsvFormat(){
        MockMultipartFile firstFile = new MockMultipartFile("data", "userdata.csv", "text/csv", "some xml".getBytes());
        assertTrue(registerService.hasCsvFormat(firstFile));
    }

    @Test
    void processAndSaveData_NoUserFound(){
        MockMultipartFile firstFile = new MockMultipartFile("data", "userdata.csv", "text/csv", "some xml".getBytes());
        assertFalse(registerService.processAndSaveData(firstFile));
    }
}

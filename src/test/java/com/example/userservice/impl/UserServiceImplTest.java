package com.example.userservice.impl;

import com.example.userservice.commons.Role;
import com.example.userservice.commons.Status;
import com.example.userservice.entity.User;
import com.example.userservice.repo.UserRepo;
import com.example.userservice.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    UserRepo userRepo;

    @InjectMocks
    UserServiceImpl userService;


    @Test
    void getByUserId() {
        User user= createUser();
        when(userRepo.findById(10)).thenReturn(Optional.of(user));
        when(userRepo.findById(1)).thenReturn(Optional.ofNullable(createAdmin()));
        ResponseEntity<?> response = userService.getByUserId("10","1");
        assertEquals(HttpStatus.FOUND,response.getStatusCode());
    }

    @Test
    void getByEmail() {
        when(userRepo.findByEmail("md@bookstore.com")).thenReturn(Optional.of(createUser()));
        when(userRepo.findById(1)).thenReturn(Optional.ofNullable(createAdmin()));
        ResponseEntity<?> response = userService.getByEmail("md@bookstore.com","1");
        assertEquals(HttpStatus.FOUND,response.getStatusCode());
    }

    @Test
    void blockUser_successFully(){
        when(userRepo.findById(12)).thenReturn(Optional.ofNullable(createAdmin()));
        when(userRepo.findById(10)).thenReturn(Optional.of(createUser()));
        assertEquals(HttpStatus.OK,userService.blockUser(10,12).getStatusCode());
    }

    @Test
    void blockUser_UserNotPresent() {
        when(userRepo.findById(12)).thenReturn(Optional.ofNullable(createAdmin()));
        User user = createUser();
        user.setUserStatus(Status.ARCHIVE);
        user.setDeRegisterTime(LocalDateTime.now());
        when(userRepo.findById(10)).thenReturn(Optional.of(user));
        assertEquals(HttpStatus.NOT_FOUND,userService.blockUser(10,12).getStatusCode());

    }

    @Test
    void blockUser_AdminNotPresent(){
        User admin = createAdmin();
        admin.setCategory(Role.User);
        when(userRepo.findById(12)).thenReturn(Optional.of(admin));
        assertEquals(HttpStatus.UNAUTHORIZED,userService.blockUser(10,12).getStatusCode());
    }

    @Test
    void blockUser_error(){
        when(userRepo.findById(12)).thenReturn(Optional.ofNullable(createAdmin()));
        User savedUser = createUser();
        when(userRepo.findById(10)).thenReturn(Optional.of(createUser()));
        savedUser.setDeRegisterTime(LocalDateTime.now());
        when(userRepo.save(savedUser)).thenReturn(savedUser);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,userService.blockUser(10,12).getStatusCode());
    }

    @Test
    void unBlockUser_successFully(){
        when(userRepo.findById(12)).thenReturn(Optional.ofNullable(createAdmin()));
        User user = createUser();
        user.setUserStatus(Status.INACTIVE);
        when(userRepo.findById(10)).thenReturn(Optional.of(user));
        assertEquals(HttpStatus.OK,userService.unBlockUser(10,12).getStatusCode());
    }

    @Test
    void unBlockUser_UserNotPresent() {
        when(userRepo.findById(12)).thenReturn(Optional.ofNullable(createAdmin()));
        User user = createUser();
        user.setDeRegisterTime(LocalDateTime.now());
        when(userRepo.findById(10)).thenReturn(Optional.of(user));
        assertEquals(HttpStatus.NOT_FOUND,userService.unBlockUser(10,12).getStatusCode());

    }

    @Test
    void unBlockUser_AdminNotPresent(){
        User admin = createAdmin();
        admin.setCategory(Role.User);
        when(userRepo.findById(12)).thenReturn(Optional.of(admin));
        assertEquals(HttpStatus.UNAUTHORIZED,userService.unBlockUser(10,12).getStatusCode());
    }

    @Test
    void unBlockUser_error(){
        when(userRepo.findById(12)).thenReturn(Optional.ofNullable(createAdmin()));
        User userToPass = createUser();
        userToPass.setUserStatus(Status.INACTIVE);
        when(userRepo.findById(10)).thenReturn(Optional.of(userToPass));
        User savedUser = createUser();
        savedUser.setDeRegisterTime(LocalDateTime.now());
        when(userRepo.save(savedUser)).thenReturn(savedUser);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,userService.unBlockUser(10,12).getStatusCode());
    }

    @Test
    void getInactiveUser(){
        when(userRepo.findAllInactiveUsers(Status.INACTIVE.ordinal(),Role.User.name())).thenReturn(new ArrayList<User>());
        assertTrue(userService.getInactiveUser().isEmpty());
    }

    @Test
    void saveUpdatedUser(){
        User user = createUser();
        userService.saveUpdatedUser(user);
        verify(userRepo,times(1)).save(user);
    }

    private User createUser() {
        return User.builder()
                .userid(10)
                .first_name("md")
                .last_name("MD")
                .password("123344")
                .email("md@bookstore.com")
                .category(Role.User)
                .userStatus(Status.ACTIVE)
                .build();
    }

    private User createAdmin() {
        return User.builder()
                .userid(12)
                .first_name("md")
                .last_name("MD")
                .password("123344")
                .email("md@bookstore.com")
                .category(Role.Admin)
                .userStatus(Status.ACTIVE)
                .build();
    }
}
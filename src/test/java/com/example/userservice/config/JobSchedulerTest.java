package com.example.userservice.config;

import com.example.userservice.commons.Status;
import com.example.userservice.entity.User;
import com.example.userservice.service.UserService;
import com.example.userservice.utility.Converter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class JobSchedulerTest {

    @Mock
    UserService userService;

    @InjectMocks
    JobScheduler jobScheduler;
    @Test
    void everyDayTask() {
        User user = createUser();
        user.setUserStatus(Status.ARCHIVE);
        when(userService.getInactiveUser()).thenReturn(createUserList(createUser()));
        jobScheduler.everyDayTask();
        verify(userService,times(1)).saveUpdatedUser(user);
    }

    @Test
    void everyDayTask_success() {
        User user = createUser();
        user.setDeRegisterTime(Converter.convertToUTC(LocalDateTime.now()));
        when(userService.getInactiveUser()).thenReturn(createUserList(user));
        user.setUserStatus(Status.ARCHIVE);
        jobScheduler.everyDayTask();
        verify(userService,times(0)).saveUpdatedUser(user);
    }

    private List<User> createUserList(User user) {
        List<User> userList = new ArrayList<>();
        userList.add(user);
        return userList;
    }

    private User createUser() {
        return User.builder()
                .userid(1)
                .email("email")
                .first_name("firstName")
                .userStatus(Status.INACTIVE)
                .deRegisterTime(Converter.convertToUTC(LocalDateTime.of(2024,01,01,02,24)))
                .build();
    }
}